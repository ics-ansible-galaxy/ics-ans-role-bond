# ics-ans-role-bond

Ansible role to create and bond interfaces for both debian and centos systems

Will configure interfaces for bonding and create if needed multiple bondings. For the bond the mac adress an be specified if not it will use the first connected interface.

The current switch set up can't handle frames over 9216 so only set mtu as maxium of 9000 for jumbo frames.

## Role Variables

```yaml
bond_master: []
# - name: "bond0"
#   device: "bond0"
#   mtu: "500"
#   ip: "{{ ansible_default_ipv4.address }}"
#   gw: "{{ csentry_interfaces[0].gateway }}"
#   dns1: "123.321.123.1"
#   dns2: 144.44.4.4
#   domain: "cslab.esss.lu.se"
#   mac: "a0:36:9f:0f:b1:70"
#   interfaces:
#     - name: enp216s0f0
#       device: enp216s0f0
#       mtu: 9000
#       master: bond0
#     - name: enp216s0f1
#       device: enp216s0f1
#       mtu: 9000
#       master: bond0
# - name: "bond1"
#   device: "bond1"
#   mtu: "500"
#   ip: 111.111.111.111
#   gw: 123.123.123.123
#   dns1: "8.8.8.8"
#   dns2: 8.8.8.7
#   domain: "tn.esss.lu.se"
#   interfaces:
#     - name: enp216s0f2
#       device: enp216s0f2
#       mtu: 9000
#       master: bond0
#     - name: enp216s0f3
#       device: enp216s0f3
#       mtu: 9000
#       master: bond0
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-bond
```

## License

BSD 2-clause
